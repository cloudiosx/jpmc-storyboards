//
//  SchoolViewController.swift
//  jpmc-storyboards
//
//  Created by John Kim on 3/10/22.
//

import UIKit

class SchoolViewController: UIViewController {
    
    // MARK: - Properties
    
    var school: School?
    var score = [Score]()
    var mathSATScore: String?
    var readingSATScore: String?
    var writingSATScore: String?
    var testTakers: String?
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var numberOfTestTakers: UILabel!
    @IBOutlet weak var mathScore: UILabel!
    @IBOutlet weak var readingScore: UILabel!
    @IBOutlet weak var writingScore: UILabel!
    
    // MARK: - Lifecycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureJSON()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        mathScore.text = "Math SAT score is " + (mathSATScore?.description ?? "not available")
        readingScore.text = "Reading SAT score is " + (readingSATScore?.description ?? "not available")
        writingScore.text = "Writing SAT score is " + (writingSATScore?.description ?? "not available")
        numberOfTestTakers.text = "Number of test takers are " + (testTakers?.description ?? "not available")
    }
    
    // MARK: - Methods
    
    func configureJSON() {
        do {
            let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!
            let data = try Data(contentsOf: url)
            self.parse(json: data)
        } catch {
            print("Error! Unable to parse list of NYC Schools")
        }
    }
    
    func parse(json: Data) {
        do {
            score = try JSONDecoder().decode([Score].self, from: json)
        } catch {
            print(error.localizedDescription)
        }
        
        self.getScores()
    }
    
    func getScores() {
        
        guard let schoolName = school?.schoolName else { return }
        
        for schoolDetail in score {
            if let schoolDetailSchoolName = schoolDetail.schoolName {
                if schoolName.lowercased().contains(schoolDetailSchoolName.lowercased()) {
                    guard let math = schoolDetail.math else { return }
                    guard let reading = schoolDetail.reading else { return }
                    guard let writing = schoolDetail.writing else { return }
                    guard let takers = schoolDetail.testTakers else { return }
                    mathSATScore = math
                    readingSATScore = reading
                    writingSATScore = writing
                    testTakers = takers
                    return
                }
            } else {
                print("Cannot find SAT scores from that school")
            }
        }
    }
    
    func configureUI() {
        title = school?.schoolName
    }
}
