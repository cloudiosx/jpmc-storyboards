### JPMC - iOS Coding Challenge: NYC Schools

# Introduction

I implemented an MVC (Model View Controller) architecture, and I incorporated multi-threading to demonstrate modern asycnhronous programming techniques by using DispatchQueue in the main view controller to make the application more efficient, which makes the JSON parsing occur in the background thread as opposed to the main thread. Furthermore, I neatly displayed the list of school names using a table view controller embedded in a navigation controller and displayed SAT score information of the specific school that the user selected using a view controller in the navigation controller stack and UILabels embedded in a vertical stack view. 

# Instructions to Run

To run the project, you can follow the steps below: 

<ol>
  <li>Git clone the repo into your local machine using your terminal</li>
  <li>Set the product destination to a device of your choice</li>
  <li>Run the project by clicking on either the play button on the top-left corner in the Xcode code editor or using the keyboard shortcut (CMD + r)</li>
</ol>

# Screenshots

![Home](https://res.cloudinary.com/dbtsjperv/image/upload/v1646938651/Screen_Shot_2022-03-10_at_1.54.55_PM_crx7gs.png)
![Specific school details](https://res.cloudinary.com/dbtsjperv/image/upload/v1646938651/Screen_Shot_2022-03-10_at_1.55.07_PM_ppfg30.png)

# Conclusion

Thank you so much for your patience, time, and providing me with a chance to do this exercise. I thoroughly enjoyed doing it and had a lot of fun working on it. 
