//
//  Score.swift
//  jpmc-storyboards
//
//  Created by John Kim on 3/10/22.
//

struct Score: Codable {
    // MARK: - Properties
    
    var math: String?
    var reading: String?
    var writing: String?
    var schoolName: String?
    var testTakers: String?
    
    // MARK: - CodingKey
    
    enum CodingKeys: String, CodingKey {
        case math = "sat_math_avg_score"
        case reading = "sat_critical_reading_avg_score"
        case writing = "sat_writing_avg_score"
        case schoolName = "school_name"
        case testTakers = "num_of_sat_test_takers"
    }
}
