//
//  ViewController.swift
//  jpmc-storyboards
//
//  Created by John Kim on 3/10/22.
//

import UIKit

class ViewController: UITableViewController {
    
    // MARK: - Properties
    
    var schools = [School]()
    
    // MARK: - Lifecycles

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureJSON()
        configureUI()
    }
    
    // MARK: - Methods
    
    func configureJSON() {
        do {
            DispatchQueue.global(qos: .userInitiated).async {
                if let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") {
                    if let data = try? Data(contentsOf: url) {
                        self.parse(json: data)
                    }
                }
            }
        } catch {
            print("Error! Unable to parse list of NYC Schools")
        }
    }
    
    func parse(json: Data) {
        do {
            schools = try JSONDecoder().decode([School].self, from: json)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func configureUI() {
        title = "NYC High Schools"
    }
}

// MARK: - Table View Methods

extension ViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let school = schools[indexPath.row]
        cell.textLabel?.text = school.schoolName
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let svc = self.storyboard?.instantiateViewController(withIdentifier: "SchoolViewController") as! SchoolViewController
        svc.school = schools[indexPath.row]
        navigationController?.pushViewController(svc, animated: true)
    }
}

