//
//  School.swift
//  jpmc-storyboards
//
//  Created by John Kim on 3/10/22.
//

struct School: Codable {
    // MARK: - Properties
    
    var schoolName: String?
    
    // MARK: - CodingKey
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
    }
}
